//
//  String+Extension.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 19.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import Foundation

extension String {
    var trimmed: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
