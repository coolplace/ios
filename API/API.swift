//
//  API.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 19.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class API: NSObject {
    class func login(email: String, password: String, completion: @escaping (Error?, _ success: Bool) -> Void) {
        let url = URLs.login
        let parameters = [
            "email": email,
            "password": password
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .failure(let error):
                    completion(error, false)
                    print(error)
                case .success(let value):
                    let json = JSON(value)
                    let token = json["data"]["tokens"]["accessToken"].string
                    print("token: \(token!)")
                    
                    // save api token to UserDefaults
                    helper.saveToken(token: token!)
                    
                    
                    completion(nil, true)
                }
        }
    }
    
    class func register(name: String, email: String, password: String, completion: @escaping (Error?, _ success: Bool) -> Void) {
        let url = URLs.register
        let parameters = [
            "username": name,
            "email": email,
            "password": password,
            "passwordConfirmation": password
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .failure(let errors):
                    completion(errors, false)
                    print(errors)
                case .success(let value):
                    let json = JSON(value)
                    let token = json["data"]["tokens"]["accessToken"].string
                    print("token: \(token!)")
                    
                    // save api token to UserDefaults
                    helper.saveToken(token: token!)
                    
                    completion(nil, true)
                }
        }
    }
}

