//
//  FieldValidations.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 25.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import Foundation
import UIKit

class fieldValidations: NSObject {
    
    class func validateEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx) 
        return emailTest.evaluate(with: email)
    }
    
    
    //    length 6 to 16.
    //    One Alphabet in password.
    //    One Special Character in password.
    class func validatePassword(password: String) -> Bool {
        let passwordRegEx = "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{6,16}"
        let passwordTest = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        let result = passwordTest.evaluate(with: password)
        return result
    }
    
    class func showAlertMessage(title: String? = nil, _ message: String) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            let alert = UIAlertController(title: "Ошибка", message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            topController.present(alert, animated: true, completion: nil)
        }
    }
    
}
