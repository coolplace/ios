//
//  Config.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 19.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import Foundation

struct URLs {
    static let main = "https://api.coolplace.io/v1/"
    
    /// POST {email, password}
    static let login = main + "auth"
    
    /// POST {name, email, password}
    static let register = main + "registration"
    
    static let bars = main + "category/1/places"
    
    
}
