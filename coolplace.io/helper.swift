//
//  Helper.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 20.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit

class helper: NSObject {
    
    class func restartApp() {
        guard let window = UIApplication.shared.keyWindow else { return }
        
        let sb = UIStoryboard(name: "Main", bundle: nil)
        
        var vc: UIViewController
        if getToken() == nil {
            // go to auth screen
            vc = sb.instantiateInitialViewController()!
        } else {
            // go to main screen
            vc = sb.instantiateViewController(withIdentifier: "main")
        }
        
        window.rootViewController = vc
        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromTop, animations: nil, completion: nil)
    }
    
    class func saveToken(token: String) {
        // save token to UserDefaults
        let def = UserDefaults.standard
        def.setValue(token, forKey: "accessToken")
        def.synchronize()
        
        restartApp()
    }
    
    class func getToken() -> String? {
        let def = UserDefaults.standard
        return def.object(forKey: "accessToken") as? String
    }
    
    
}

