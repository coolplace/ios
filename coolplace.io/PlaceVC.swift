//
//  PlaceVC.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 02.11.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PlaceVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate {
    
    var selectedUrl: String?
    var selectedTitle: String?
    
    @IBOutlet weak var tableViewPlaces: UITableView!
    
    var places = [Place]()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        let place: Place
        place = places[indexPath.row]
        
        cell.namePlace.text = place.title
        cell.nameStreet.text = place.street
        cell.nameSubway.text = place.subway
        
        Alamofire.request(place.image!).responseImage { response in
            debugPrint(response)
            
            if let image = response.result.value {
                cell.logoPlace.image = image
            }
        }
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PlaceViewController {
            destination.place = places[(tableViewPlaces.indexPathForSelectedRow?.row)!]
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let titleToDisplay = selectedTitle {
            self.navigationItem.title = titleToDisplay
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewPlaces.delegate = self
        tableViewPlaces.dataSource = self
        
        Alamofire.request(selectedUrl!).responseJSON { response in
            if let json = response.result.value as! [String: Any]?  {
                
                let jsonData = json["data"] as! [[String: Any]]?
                let placesArray: NSArray = jsonData! as NSArray
                
                for i in 0..<placesArray.count {
                    
                    // adding place values to the place list
                    self.places.append(Place(
                        title: (placesArray[i] as AnyObject).value(forKey: "title") as? String,
                        description: (placesArray[i] as AnyObject).value(forKey: "description") as? String,
                        street: (placesArray[i] as AnyObject).value(forKey: "street") as? String,
                        subway: ((placesArray[i] as AnyObject).value(forKey: "subway") as? String)!,
                        image: ((placesArray[i] as AnyObject).value(forKey: "image") as? String)!
                    ))
                }
                
                // displaying data in tableview
                self.tableViewPlaces.reloadData()
            }
            
        }
        
        self.tableViewPlaces.reloadData()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
}


