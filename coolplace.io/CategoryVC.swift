//
//  CollectionVC.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 06.11.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var imageArray = [UIImage]()
    var categoryArray = [String]()
    var arrayrOfIDs = [String]()
    let urls = [
        "https://api.coolplace.io/v1/category/1/places",
        "https://api.coolplace.io/v1/category/2/places",
        "https://api.coolplace.io/v1/category/3/places",
        "https://api.coolplace.io/v1/category/4/places",
        "https://api.coolplace.io/v1/category/5/places",
        "https://api.coolplace.io/v1/category/6/places",
    ]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        imageArray = [#imageLiteral(resourceName: "001-food-2"), #imageLiteral(resourceName: "005-food-1"), #imageLiteral(resourceName: "003-cheers"), #imageLiteral(resourceName: "006-food"), #imageLiteral(resourceName: "002-monuments"), #imageLiteral(resourceName: "004-game")]
        categoryArray = ["Бары", "Кафе", "Рестораны", "Магазины", "Культура", "Спорт"]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CategoryCVC
        
        cell.categoryImage.image = imageArray[indexPath.row]
        cell.categoryName.text = categoryArray[indexPath.row]
        
        cell.contentView.backgroundColor = UIColor.white
        cell.contentView.layer.cornerRadius = 8.0
        cell.contentView.layer.borderWidth = 0.5
        cell.contentView.layer.borderColor = UIColor.lightGray.cgColor
        cell.contentView.layer.masksToBounds = true
        cell.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 0)
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOpacity = 0.8
        cell.layer.masksToBounds = false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let displayListPlace = storyboard?.instantiateViewController(withIdentifier: "bars") as! PlaceVC
        displayListPlace.selectedTitle = categoryArray[indexPath.row]
        displayListPlace.selectedUrl = urls[indexPath.row]
        
        self.navigationController?.pushViewController(displayListPlace, animated: true)
    }
    
}
