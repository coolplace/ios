//
//  TableViewCell.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 03.11.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class TableViewCell: UITableViewCell {

    @IBOutlet weak var backgroundCardView: UIView!
    @IBOutlet weak var logoPlace: UIImageView!
    @IBOutlet weak var namePlace: UILabel!
    @IBOutlet weak var nameStreet: UILabel!
    @IBOutlet weak var nameSubway: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundCardView.backgroundColor = UIColor.white
        backgroundCardView.layer.cornerRadius = 8.0
        backgroundCardView.layer.masksToBounds = true
        backgroundCardView.layer.shadowColor = UIColor.black.withAlphaComponent(0.2).cgColor
        backgroundCardView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backgroundCardView.layer.shadowOpacity = 1.0
        backgroundCardView.layer.shadowRadius = 2.0
        backgroundCardView.layer.shadowOpacity = 0.8
        
        
    }

}

