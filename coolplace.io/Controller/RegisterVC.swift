//
//  RegisterVC.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 19.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RegisterVC: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passwordAgainTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func registerPressed(_ sender: UIButton) {
        
        let validEmail = fieldValidations.validateEmail
        let validPass = fieldValidations.validatePassword
        
        guard
            let name = nameTF.text?.trimmed, !name.isEmpty,
            let email = emailTF.text?.trimmed, !email.isEmpty,
            let password = passwordTF.text, !password.isEmpty,
            let passwordAgain = passwordAgainTF.text, !passwordAgain.isEmpty
        else {
            fieldValidations.showAlertMessage(title: "Ошибка", "Заполните все поля")
            return
        }
        
        guard password == passwordAgain else {
            fieldValidations.showAlertMessage(title: "Ошибка", "Пароли не совпадают")
            return
        }
        
        if !validPass(password) {
            fieldValidations.showAlertMessage(title: "Ошибка", "В пароле должно быть минимум 6 символов")
            return
        }
        
        if !validEmail(email) {
            fieldValidations.showAlertMessage(title: "Ошибка", "Некорректный email")
            return
        }
        
        API.register(name: name, email: email, password: password) { (error: Error?, success: Bool) in
            if success {
                print("Register succeed!")
            } else {
                print(error as Any)
            }
        }
    }
}
