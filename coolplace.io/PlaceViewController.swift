//
//  PlaceViewController.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 26.11.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class PlaceViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descrip: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var subway: UILabel!
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true)
    }
    
    var place: Place?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (name) != nil {
            self.navigationItem.title = place?.title
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = place?.title
        descrip.text = place?.description
        address.text = place?.street
        subway.text = place?.subway
        
        
        let imageURL = place?.image
        Alamofire.request(imageURL!).responseImage { response in
            if let image = response.result.value {
                self.img.image = image
            }
        }
    }


}
