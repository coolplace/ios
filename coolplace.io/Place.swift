//
//  Place.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 26.11.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

class Place {
    
    var title: String?
    var description: String?
    var street: String?
    var subway: String?
    var image: String?
    
    init(title: String?, description: String?, street: String?, subway: String, image: String) {
        self.title = title
        self.description = description
        self.street = street
        self.subway = subway
        self.image = image 
    }
    
}
