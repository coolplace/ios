//
//  CategoryCVC.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 12.11.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit

class CategoryCVC: UICollectionViewCell {
    
    @IBOutlet weak var categoryImage: UIImageView!
    @IBOutlet weak var categoryName: UILabel!
    
    
}
