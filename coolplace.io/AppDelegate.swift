//
//  AppDelegate.swift
//  coolplace.io
//
//  Created by Dmitry Korchagin on 08.10.2017.
//  Copyright © 2017 coolplace.io. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let token = helper.getToken() {
            print("Token: \(token)")
            // skip auth screen
            // and go to main screen
            
            let tab = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "main")
            window?.rootViewController = tab
        }
        
        return true
    }


}

